# panda_capacity package

`panda_capacity` package is an example implementation of the task space capacity calculation for for Franka Emika Panda robot in a form of a catkin ros package.

It uses the library [pinocchio](https://github.com/stack-of-tasks/pinocchio) for calculating robot kinematics.
## Dependencies
For visualizing the polytopes in RVIZ you will need to install the [jsk-rviz-plugin](https://github.com/jsk-ros-pkg/jsk_visualization)

```sh
sudo apt install ros-*-jsk-rviz-plugins # melodic/kinetic... your ros version
```

The code additionally depends on the pinocchio library. 

### Pinocchio already installed
If you do already have pinocchio installed simply install the the pip package `pycapacity`
```
pip install pycapacity
```
And you're ready to go!

### Installing pinocchio

If you dont have pinocchio installed we suggest you to use Anaconda. Create the environment will all dependencies:

```bash
conda env create -f env.yaml 
conda activate panda_capacity  # activate the environment
```

Once the environment is activated you are ready to go.

## Create your own catkin workspace

Then create new catkin workspace:
```bash
mkdir ~/capacity_ws && cd ~/capacity_ws/
mkdir src && cd src
```
Clone the projects:
```shell
git clone https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/panda_capacity.git
```
Clone the Franka description package for visualisation 
```shell
git clone https://gitlab.inria.fr/auctus-team/components/robots/panda/franka_description.git
```

Finally you can build the workspace
```shell
cd ..
catkin build
```
And you should be ready to go!



### One panda simulation
Once when you have everything installed you will be able to run the interactive simulations and see the polytope being visualised in real-time.

<img src="images/one_panda.png" height="250px">

To see a simulation with one panda robot and its force and velocity manipulatibility ellipsoid and polytope run the command in the terminal.

```shell
source ~/capacity_ws/devel/setup.bash 
roslaunch panda_capacity one_panda.launch
```
> If using anaconda, don't forget to call <br>
> `conda activate panda_capacity` <br> 
> before you call `roslaunch`
