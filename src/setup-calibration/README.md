# World_camera_calibration

This is a package to calibrate the position of a camera relatively to a robot using Apriltag markers 

# Robot to calibration tag calibration.

First you need to calibrate the position of a `calibration tag` relatively to the robot. This `calibration_tag` should be big enough to be precisely seen by the robot.

1. Print a tag and fix it on the robot table. 
2. Fix the pointer tip to the panda robot
3. Launch a robot with the pointer activated in the robot_description
4. Run `roslaunch world_camera_calibration world_tag_calibration.launch`
5. Put the robot in gravity compensation mode and follow the instruction in the termianl
6. Once you've acquired 4 points it will save the calibration data in the `config` folder 

# Robot to world_camera_calibration

This package offer several option :

1. Continuously track the pose of the camera relatively to the robot
2. Calibrate and save the pose of the camera relatively to the robot
3. Load a previous calibration of the pose of the camera relatively to the robot

## Continuous tracking 

```bash
```

## Calibrate and save 

`roslaunch  world_camera_calibration world_camera_calibration.launch calibration_iteration_number:=10`

This will compute the pose of the camera relatively to the robot a number of time equal to `calibration_iteration_number` and do an average of the camera pose. The pose of the camera is saved in the `config` folder

## Load a previous calibration

`roslaunch  world_camera_calibration world_camera_calibration.launch calibration_iteration_number:=0`
