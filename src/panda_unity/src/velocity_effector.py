#!/usr/bin/env python3
import rospy
import pinocchio as pin
import numpy as np
from scipy import signal
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Twist, TwistStamped
from pynocchio import RobotWrapper

# loading the root urdf from robot_description parameter
robot = RobotWrapper(xml_path=rospy.get_param("/panda/robot_description") , tip="panda_link8")

# initial position and velocity
q = np.zeros((7,))
v = np.zeros((7,))


def callback(data):
    global v,q
    q = data.position
    v = data.velocity


def velocity_calculator():
    global v,q

    rospy.init_node('velocity_effector', anonymous=True)
    #le node écoute le joint_state et publie le vecteur de vitesse de l'effecteur
    sub = rospy.Subscriber('/panda/joint_states', JointState, callback, queue_size=2)
    pub = rospy.Publisher ('velocity_effector', Twist, queue_size=2)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        #m = TwistStamped()
        #m.header.frame_id="world"
        sol = Twist()
        
        #on définit la jacobienne par pynocchio
        J = robot.jacobian(q, frame_align=pin.LOCAL, frame_name="panda_link8")

        #on calcule le vecteur vitesse de l'effecteur 
        viteff = J.dot(v)

        #on publie les résultats sous forme d'un Twist
        sol.linear.x, sol.linear.y, sol.linear.z = viteff[0], viteff[1], viteff[2]
        sol.angular.x, sol.angular.y, sol.angular.z = viteff[3], viteff[4], viteff[5]

        #m.twist=sol
        pub.publish(sol)
        print("la vitesse de l'effecteur : ", viteff)

        rate.sleep()

if __name__ == '__main__':
    velocity_calculator()